from django.db import models

from commons.constants import PRICING_STRATEGY, EDLP
from commons.models import AbstractModel


class Product(AbstractModel):
    name = models.CharField(
        max_length=120, blank=True, null=True,
        verbose_name='Название'
    )
    description = models.TextField(
        blank=True, null=True,
        verbose_name='Описание'
    )
    item_number = models.CharField(
        max_length=8, unique=True, verbose_name='Артикул'
    )
    price = models.DecimalField(
        max_digits=8, decimal_places=2, verbose_name='Цена'
    )
    pricing_strategy = models.CharField(
        choices=PRICING_STRATEGY, max_length=10, default=EDLP,
        verbose_name='Стратегия ценообразования'
    )
    wb_competitors = models.CharField(
        max_length=120, blank=True, null=True,
        verbose_name='Цена у конкурентов WB'
    )
    available_from_competitors = models.BooleanField(
        default=True, verbose_name='Есть в наличии у конкурентов WB'
    )

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    def __str__(self):
        return f'{self.item_number}'
