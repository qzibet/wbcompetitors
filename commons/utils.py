import logging

import requests


class RequestBuilder:
    base_url = None

    def __init__(self, method, base_url=None):
        if not self.base_url and base_url:
            self.base_url = base_url

        self.session = requests.Session()
        self.method = self.method_mapper.get(method, self.session.get)
        self.path = None
        self.data = None
        self.params = None
        self.headers = None

    @property
    def method_mapper(self):
        return {
            'POST': self.session.post,
            'GET': self.session.get,
            'PUT': self.session.put,
            'PATCH': self.session.patch,
            'DELETE': self.session.delete,
        }

    @property
    def full_path(self):
        base = self.base_url or ''
        path = self.path or '/'
        return f'{base}{path}'

    @classmethod
    def post(cls):
        return cls('POST')

    @classmethod
    def get(cls):
        return cls('GET')

    @classmethod
    def put(cls):
        return cls('PUT')

    @classmethod
    def patch(cls):
        return cls('PATCH')

    @classmethod
    def delete(cls):
        return cls('DELETE')

    def with_base_url(self, base_url):
        self.base_url = base_url
        return self

    def with_path(self, path):
        self.path = path
        return self

    def with_data(self, data):
        if isinstance(self.data, dict):
            self.data.update(data)
            return self

        self.data = data
        return self

    def with_params(self, params):
        if isinstance(self.params, dict):
            self.params.update(params)
            return self

        self.params = params
        return self

    def with_headers(self, headers):
        if isinstance(self.headers, dict):
            self.headers.update(headers)
            return self

        self.headers = headers
        return self

    def send(self):
        kwargs = {}
        if self.data:
            kwargs['data'] = self.data
        if self.params:
            kwargs['params'] = self.params
        if self.headers:
            kwargs['headers'] = self.headers

        response = self.method(self.full_path, **kwargs)

        logging.debug(f'raw response from server: {response.text}')

        try:
            data = response.json()
        except ValueError:
            data = {'detail': response.text}
            logging.debug(f'[HTTP {response.status_code}] ({self.full_path}) {data}')
        return data
