## Запуск проекта на Linux(Ubuntu/Debian)

1. Нужно создать виртуальное окружения и установить пакеты
   * ```sudo apt-get install python3.8 python3.8-venv```
   * создание ```python3.8 -m venv .venv```
   * активация ```. .venv/bin/activate```
   * установка пакетов ```pip install -r requirements/base.txt```

2. Нужно создать файл `.env` в папке с проектом
3. Скопировать все что есть в `.env.example` в новый файл `.env`
4. Запустить проект ```python manage.py runserver```