from ast import literal_eval

from rest_framework import serializers

from commons.constants import EDLP
from products.models import Product


class ParseProductFile(serializers.Serializer):
    name = serializers.CharField(max_length=120, required=True)
    file = serializers.FileField(required=True)

    class Meta:
        fields = (
            'name',
            'file'
        )


class ProductList(serializers.ModelSerializer):
    parse_date = serializers.SerializerMethodField()
    new_price = serializers.SerializerMethodField()

    class Meta:
        model = Product
        exclude = (
            'created_at',
            'updated_at'
        )

    def get_parse_date(self, obj):
        return obj.updated_at or obj.created_at

    def get_new_price(self, obj):
        prices = [int(price) for price in literal_eval(obj.wb_competitors)]
        if obj.pricing_strategy == EDLP:
            return min(prices) - int(min(prices ) * 0.01)
        else:
            return int(sum(prices) / len(prices))


class ProductRetrieve(ProductList):
    class Meta:
        model = Product
        fields = (
            'parse_date',
            'new_price',
            'available_from_competitors',
        )
