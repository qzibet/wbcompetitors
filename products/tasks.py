import logging

from huey.contrib.djhuey import task

from products.managers import ProductManager


# @task() TODO: uncomment `@task()` if you want async task
def create_or_update_products(file: bytes):
    try:
        ProductManager.parse_xlsx(file)
    except Exception as e:
        logging.error(e)
