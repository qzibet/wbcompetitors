from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.response import Response

from commons.paginations import LargeListPagination
from products.models import Product
from products.serializers import ParseProductFile, ProductList, ProductRetrieve
from products.tasks import create_or_update_products


class ParseFile(generics.CreateAPIView):
    serializer_class = ParseProductFile

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        file = request.FILES['file'].file
        create_or_update_products(file)
        return Response(
            {'title': 'Файл успешно принят'},
            status=status.HTTP_201_CREATED,
        )


class ProductListAPIView(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductList
    pagination_class = LargeListPagination


class ProductRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = ProductRetrieve

    def get_object(self):
        return get_object_or_404(
            Product, item_number=self.kwargs.get('item_number')
        )
