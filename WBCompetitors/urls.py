from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.conf import settings

from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from products import views


schema_view = get_schema_view(
    openapi.Info(
        title="WBCompetitors",
        default_version='v1',
        description="API Documentation",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
)

api_v1 = [
    path('products/', include([
        path('', views.ProductListAPIView.as_view()),
        path('<int:item_number>/', views.ProductRetrieveAPIView.as_view()),
        path('parse-file/', views.ParseFile.as_view()),
    ]),)
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(api_v1))
]

if settings.DEBUG:
    urlpatterns += [
        path(
            'swagger/', schema_view.with_ui('swagger', cache_timeout=0),
            name='schema-swagger-ui'
        )
    ]
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT
    )
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )