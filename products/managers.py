import requests
import openpyxl

from ast import literal_eval
from typing import List

from django.conf import settings
from django.db import IntegrityError

from commons.constants import AVERAGE, EDLP
from products.models import Product


class ProductManager:

    @classmethod
    def parse_product(
            cls, item_number: int,
    ):
        products = cls.get_product_data(item_number)
        product = [
            Product(
                price=(
                        str(product.get('salePriceU'))[:-2]
                        or str(product.get('priceU'))[:-2]
                ),
                item_number=item_number,
                name=product.get('name'),
                available_from_competitors=bool(product['sizes'][0].get('stocks'))
            )
            for product in products if product.get('id') == item_number
        ][0]

        return product

    @classmethod
    def parse_xlsx(cls, file: bytes):
        """
        row[0] - Артикул
        row[1] - Стратегия ценообразования
        row[2] - Конкуренты WB
        row[3] - Дата парсинга
        row[4] - В наличии у конкурентов
        row[5] - Новая цена
        """
        workbook = openpyxl.load_workbook(file)
        ws = workbook.active
        for row in ws.iter_rows(
                min_row=2,
                max_col=6,
                values_only=True
        ):
            competitors_list = []
            try:
                item_numbers = literal_eval(row[2])
                if isinstance(item_numbers, int):
                    competitors_list.append(cls.parse_product(item_numbers))

                elif isinstance(item_numbers, tuple):
                    for item in item_numbers:
                        competitors_list.append(cls.parse_product(item))

                our_product = cls.parse_product(int(row[0]))
                Product(
                    item_number=int(row[0]),
                    wb_competitors=(
                        f'{[item.price for item in competitors_list]}'
                    ),
                    pricing_strategy=row[1][:8],
                    price=our_product.price,
                    available_from_competitors=cls.exists_at_competitors(
                        competitors_list
                    )
                ).save()

            except IntegrityError:
                our_product = cls.parse_product(int(row[0]))
                product = Product.objects.filter(item_number=int(row[0])).first()
                product.wb_competitors = (
                    f'{[item.price for item in competitors_list]}'
                )
                product.pricing_strategy = (
                    AVERAGE if row[1][:8] == AVERAGE else EDLP
                )
                product.price = our_product.price
                product.save()

            except SyntaxError:
                continue

    @staticmethod
    def get_product_data(item_number: int):
        competitor = settings.COMPETITORS_URL[0]
        url = competitor.format(
            item_number=item_number, dest=settings.URL_DEST
        )
        data = requests.request('GET', url).json()
        return data['data']['products']

    @staticmethod
    def save_product(products: List[Product]):
        Product.objects.bulk_create(products)

    @staticmethod
    def exists_at_competitors(products: List[Product]):
        existing = [item.available_from_competitors for item in products]
        return True if True in existing else False

